<?php include("src/include/header.php"); ?>

<section id="section_paymentResult">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-8 mx-auto">
				<div class="content text-center">
					<h5>付款成功</h5>
					<p>感謝您的購買，您的訂單已成功付款。</p>	
					<p>
						<span>訂單編號：</span>
						<span>12345678</span>
					</p>
					<p>
						<span>抽獎序號：</span>
						<span>12345678</span>
					</p>
					<i class="fas fa-check-circle fa-6x success-color pt-4 pb-4"></i>
					<div class="color-white">
						<a href="index.php" class="btn cus-btn">
							返回首頁
						</a>
					</div>
				</div>

				<div class="content text-center d-none">
					<h5>付款失敗</h5>
					<p>該次訂單付款未成功，請至購買頁面重新下單</p>	
					<p>
						<span>訂單編號：</span>
						<span>12345678</span>
					</p>
					<i class="fas fa-times fa-6x fail-color pt-4 pb-4"></i>
					<div class="color-white">
						<a href="index.php" class="btn cus-btn">
							返回首頁
						</a>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>


<?php include("src/include/footer.php"); ?>

