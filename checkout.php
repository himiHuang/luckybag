<?php include("src/include/header.php"); ?>

<section id="section_checkout">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-8">
				<div class="title pb-4 pl-2">
				  <h5>結帳資訊</h5>
				</div>

				<div class="content d-flex flex-column">
					<div class="order-block payment pb-4">
						<h6 class="pb-3">海鮮福袋989元, 4盒免運</h6>
						<div class="form-group">
							<label for="luckyBagNumber">選擇福袋數量</label>
							    <select class="form-control" id="luckyBagNumber">
							      <option>1盒 運費250元</option>
							      <option>2盒 運費190元</option>
							      <option>3盒 運費130元</option>
							      <option>4盒 免運</option>
							    </select>
						</div>
					</div>

					<div class="order-block payment pb-4">
						<h6 class="pb-3">付款方式</h6>
						<div class="form-group">
							<input class="cus-form-check-input" type="radio" name="debtCard" id="CreditCard" value="option1" checked>
							<label class="form-check-label" for="CreditCard">
								信用卡付款
							 </label>
						</div>
						<div class="form-group">
							<input class="cus-form-check-input" type="radio" name="debtCard" id="ATM" value="option1" checked>
							<label class="form-check-label" for="ATM">
								ATM轉帳
							 </label>
						</div>
					</div>

					<div class="order-block buyer-info pb-4">
						<h6 class="pb-3">購買人資料</h6>
						<div class="form-group">
							<label for="inputName">姓名</label>
							<input type="text" class="form-control" id="inputName" placeholder="請輸入購買人姓名">
				   		</div>
						<div class="form-group">
							<label for="inputEmail">Email</label>
							<input type="email" class="form-control" id="inputEmail" placeholder="請輸入購買人電子信箱">
				   		</div>
		   				<div class="form-group">
		   					<label for="inputTel">手機號碼</label>
		   					<input type="tel" class="form-control" id="inputTel" placeholder="請輸入購買人手機號碼">
		   		   		</div>
   		   				<div class="form-group">
   		   					<label for="inputAdress">收件地址</label>
   		   					<input type="text" class="form-control" id="inputAdress" placeholder="請輸入購買人收件地址">
   		   		   		</div>
					</div>

					<input type="submit" value="下一步" class="btn align-self-end cus-btn">

				</div>	
			</div>
			<div class="col-12 col-sm-12 col-md-4 pt-sm-4 pt-4 pt-md-0">
				<div class="title pb-4 pl-2">
				  <h6>訂單明細</h6>
				</div>
				<div class="order">
					<div class="order-item d-flex flex-column">
						<div class="d-flex justify-content-between">
							<span>超值海鮮福袋組</span>
							<span>X1</span>
						</div>
						<div class="align-self-end">NT$989</div>	
					</div>
				</div>

				<div class="total">
					<div class="total-item d-flex justify-content-between">
						<span class="forth-color">小計</span>
						<span>NT$989</span>
					</div>
					<div class="total-item d-flex justify-content-between">
						<span class="forth-color">運費</span>
						<span>NT$90</span>
					</div>
					<div class="total-item d-flex justify-content-between">
						<span class="forth-color">總計</span>
						<h5 class="second-color">NT$90</h5>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>


<?php include("src/include/footer.php"); ?>

