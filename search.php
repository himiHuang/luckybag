<?php include("src/include/header.php"); ?>

<section id="section_search">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-8 mx-auto">
				<div class="title pb-4 pl-2">
				  <h5>查詢記錄</h5>
				</div>
				<form action="post" method="post">
					<div class="content d-flex flex-column">
						<div class="order-block buyer-info pb-4">
							<h6 class="pb-3">購買人資料</h6>
							<div class="form-group">
								<label for="inputName">姓名</label>
								<input type="text" class="form-control" id="inputName" placeholder="請輸入購買人姓名">
					   		</div>
							<div class="form-group">
								<label for="inputEmail">Email</label>
								<input type="email" class="form-control" id="inputEmail" placeholder="請輸入購買人電子信箱">
					   		</div>
			   				<div class="form-group">
			   					<label for="inputTel">手機號碼</label>
			   					<input type="tel" class="form-control" id="inputTel" placeholder="請輸入購買人手機號碼">
			   		   		</div>
						</div>

						<div class="order-block buyer-info pb-4">
							<h6 class="pb-3">訂單編號查詢</h6>
							<div class="form-group">
								<label for="inputIdNumber">訂單編號查詢</label>
								<input type="text" class="form-control" id="inputIdNumber" placeholder="請輸入您購買的訂單編號">
					   		</div>
						</div>
						<div class="g-recaptcha" data-sitekey="6LfHKqMUAAAAAI6H9QtZdqwqqbNmwqDkWJnif7ug"></div>


						<input type="submit" value="下一步" class="btn align-self-end cus-btn">

					</div>	
				</form>


			</div>
		</div>
	</div>
</section>


<?php include("src/include/footer.php"); ?>

