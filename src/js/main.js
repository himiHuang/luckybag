$(".slider.main-slider").slick({
	arrows: false,
    autoplay: false,
    dots: true,
    
});


$('.product.slider-for').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: true,
	speed: 1000,
	fade: true,
	asNavFor: '.slider-nav',
	prevArrow:"<img class='a-left control-c prev slick-prev' src='./src/dist/img/arrow-left.png'>",
    nextArrow:"<img class='a-right control-c next slick-next' src='./src/dist/img/arrow-right.png'>"
 });
 $('.product.slider-nav').slick({
	slidesToShow: 5,
	//slidesToScroll: 1,
	asNavFor: '.slider-for',
	//dots: false,
	focusOnSelect: true,
 });
$(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
$( window ).resize(function() {
	setTimeout(function(){
		$(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
	}, 100);
  
});



//===============================

$("body").css("padding-top", $(".navbar").height() + "px")



 //==========================menu click outside================



$('button.navbar-toggler').click(function(){
    //if($(".navbar-collapse").hasClass("show") == true)console.log(1)
    $("header ~ *").on('click', function(e){
		if($(".navbar-collapse").hasClass("show") === true){
			e.preventDefault();
            $(".navbar-collapse").removeClass("show")
        }else{
            return true
        }

    });

});

//==============================


function scrollIn(obj, aniName, aniTime){
  $(obj).css("opacity", "0");
  $(window).scroll(function(){
    $(obj).each(function(){
      if($(window).scrollTop() + $(window).height()>= $(this).offset().top){
        var self = this;
        setTimeout(function(){
          $(self).addClass(`animated ${aniName}`).css("opacity", "1");
        }, aniTime); 
      }
    })
  }); 
  var time = 0;
  $(obj).each(function(){
    if($(window).scrollTop() + $(window).height() > $(this).offset().top){
      var self = this;
      setTimeout(function() {
          $(self).addClass(`animated ${aniName}`).css("opacity", "1");;
      }, time);
      time += aniTime;
    }
  });
}


scrollIn("section, header, footer", "fadeInUp", 100)
//==============================


setTimeout(function(){
 $('body').css("padding-top", `${$('nav').outerHeight()}px`);
 
}, 100);












