<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">

    
    <title>luckbag</title>
    <!-- Bootstrap core CSS-->
    <!--link(href='vendor/bootstrap/css/bootstrap.css', rel='stylesheet')-->
    <!-- Custom styles for this template-->
    <!--link(href='vendor/bootstrap/css/bootstrap2.css', rel='stylesheet')-->
    <!--link(href='css/bootstrap337.css', rel='stylesheet')-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
    <link href="src/dist/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet prefetch" href="src/dist/css/slick.css">
    <link rel="stylesheet prefetch" href="src/dist/css/slick-theme.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="src/css/public.css">
    <link type="text/css" rel="stylesheet" href="src/css/flipclock.css" />
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>



    <!--link(href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet prefetch')-->
  </head>
  <body>

    <nav class="pb-0 pt-0 pl-4 navbar navbar-light bg-light fixed-top">
      <span class="navbar-brand mb-0 h1">
        <img src="src/dist/img/94i抽-LOGO.svg" width="50" height="50" class="d-inline-block align-top"alt="">
      </span>
    </nav>