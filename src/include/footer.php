      <footer id="section_footer" class="main-bg-color">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="content text-white text-center">
                <ul class="list-inline link pb-2">
                  <li class="list-inline-item">
                    <a href="notice.php" class="text-white">注意事項</a> 
                  </li>
                  <li class="list-inline-item">
                    <a href="security.php"  class="text-white">隱私權條款</a> 
                  </li>
                  <li class="list-inline-item">
                    <a href="search.php"  class="text-white">查詢序號</a> 
                  </li>
                  <li class="list-inline-item">
                    <a href="https://www.facebook.com/94%E6%84%9B%E6%8A%BD-579722992515895/"  class="text-white">Facebook</a> 
                  </li>
                </ul>
                <p>本活動執行由遠東萬佳法律事務所為活動顧問另有律師見證</p>
                <p>客服電話: 0916-468876</p>
                <p class="pt-3">Copyright © <strong>MOVEON-DESIGN</strong> All right reserved 2018</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="src/dist/js/slick.js"></script>
    <script src="src/dist/js/all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="src/dist/js/flipclock.js"></script>

  </body>
</html>