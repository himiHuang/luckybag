$(".slider.main-slider").slick({
  arrows: false,
  autoplay: true,
  dots: true

});

$('.product.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  speed: 1000,
  fade: true,
  asNavFor: '.slider-nav',
  prevArrow: "<img class='a-left control-c prev slick-prev' src='./src/dist/img/arrow-left.png'>",
  nextArrow: "<img class='a-right control-c next slick-next' src='./src/dist/img/arrow-right.png'>"
});
$('.product.slider-nav').slick({
  slidesToShow: 5,
  //slidesToScroll: 1,
  asNavFor: '.slider-for',
  //dots: false,
  focusOnSelect: true
});
$(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
$(window).resize(function () {
  setTimeout(function () {
    $(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
  }, 100);
});

//===============================

$("body").css("padding-top", $(".navbar").height() + "px");

//==========================menu click outside================


$('button.navbar-toggler').click(function () {
  //if($(".navbar-collapse").hasClass("show") == true)console.log(1)
  $("header ~ *").on('click', function (e) {
    if ($(".navbar-collapse").hasClass("show") === true) {
      e.preventDefault();
      $(".navbar-collapse").removeClass("show");
    } else {
      return true;
    }
  });
});

//==============================


function scrollIn(obj, aniName, aniTime) {
  $(obj).css("opacity", "0");
  $(window).scroll(function () {
    $(obj).each(function () {
      if ($(window).scrollTop() + $(window).height() >= $(this).offset().top) {
        var self = this;
        setTimeout(function () {
          $(self).addClass('animated ' + aniName).css("opacity", "1");
        }, aniTime);
      }
    });
  });
  var time = 0;
  $(obj).each(function () {
    if ($(window).scrollTop() + $(window).height() > $(this).offset().top) {
      var self = this;
      setTimeout(function () {
        $(self).addClass('animated ' + aniName).css("opacity", "1");;
      }, time);
      time += aniTime;
    }
  });
}

scrollIn("section, header, footer", "fadeInUp", 100);



setTimeout(function(){
 $('body').css("padding-top", `${$('nav').outerHeight()}px`);
 
}, 100);


// navbar

/**
 * Scroll management
 */
$(document).ready(function () {

    // Define the menu we are working with
    var menu = $('.main-nav');

    // Get the menus current offset
    var origOffsetY = menu.offset().top;

    /**
     * scroll
     * Perform our menu mod
     */
    function scroll() {

        // Check the menus offset. 
        if ($(window).scrollTop() >= origOffsetY) {

            //If it is indeed beyond the offset, affix it to the top.
            $(menu).addClass('cus-fixed-top');

        } else {

            // Otherwise, un affix it.
            $(menu).removeClass('cus-fixed-top');

        }
    }

    // Anytime the document is scrolled act on it
    document.onscroll = scroll;

});



// timer

$(function(){
  FlipClock.Lang.Custom = { days:'Days', hours:'Hours', minutes:'Minutes', seconds:'Seconds' };
  var opts = {
    clockFace: 'DailyCounter',
    countdown: true,
    language: 'Custom'
  };  
  var countdown = 1557049560 - ((new Date().getTime())/1000); // from: 05/05/2019 05:46 pm +0800
  countdown = Math.max(1, countdown);
  $('.clock-builder-output').FlipClock(countdown, opts);
});



// slidetoggle

$(document).ready(function(){
   $(".panel").slideUp();
    $(".open-btn").click(function(){
        $(this).next(".panel").slideToggle("slow");
        $(this).text(function(i, v){
          return v === '▼點我查看更多' ? '▲ 收回內容' : '▼點我查看更多'
      });
    });
});


$(document).ready(function(){
   $(".panel").slideUp();
    $(".close-btn").click(function(){
        $(this).prev(".panel").slideToggle("slow");
        $(this).text(function(i, v){
          return v === '▼點我查看更多' ? '▲ 收回內容' : '▼點我查看更多'
      });
    });
});

// menuspy

$(document).ready(function() {
  var $menu = $(".main-nav");
  var $menu_a = $("a", $menu);
  var id = false;
  var sections = [];
  // history.pushState 改變網址卻不需要刷新頁面，當你把頁面往下拉時，加載分頁內容，網址也跟著改變

  var hash = function(h) {
    if (history.pushState) {
      history.pushState(null, null, h);
    } else {
      location.hash = h;
    }
  };

  $menu_a.click(function(event) {
    event.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $($(this).attr("href")).offset().top - $(".main-nav").height()
      },
      {
        duration: 700,
        complete: hash($(this).attr("href"))
      }
    );
  });

  $menu_a.each(function() {
    sections.push($($(this).attr("href")));
  });

  $(window).scroll(function(event) {
    var scrolling = $(this).scrollTop() + $(this).height() / 3;
    var scroll_id;
    for (var i in sections) {
      var section = sections[i];
      if (scrolling > section.offset().top) {
        scroll_id = section.attr("id");
      }
    }
    if (scroll_id !== id) {
      id = scroll_id;
      $menu_a.removeClass("active");
      $("a[href='#" + id + "']", $menu).addClass("active");
    }
  });
});

// $(document).ready(function () {
//     $(document).on("scroll", onScroll);
    
    //smoothscroll
//     $('a[href^="#"]').on('click', function (e) {
//         e.preventDefault();
//         $(document).off("scroll");
        
//         $('a').each(function () {
//             $(this).removeClass('active');
//         })
//         $(this).addClass('active');
      
//         var target = this.hash,
//             menu = target;
//         $target = $(target);
//         $('html, body').stop().animate({
//             'scrollTop': $target.offset().top+2
//         }, 500, 'swing', function () {
//             window.location.hash = target;
//             $(document).on("scroll", onScroll);
//         });
//     });
// });

// function onScroll(event){
//     var scrollPos = $(document).scrollTop();
//     $('.main-nav a').each(function () {
//         var currLink = $(this);
//         var refElement = $(currLink.attr("href"));
//         if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
//             $('.main-nav ul li a').removeClass("active");
//             currLink.addClass("active");
//         }
//         else{
//             currLink.removeClass("active");
//         }
//     });
// }


//img-slider
$('.caresoul').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
});

let $bigImage = $('#bigImage');
$('.caresoul img').on('click', function () {
  let src = this.src;
  $bigImage.attr('src', src);
});