<?php include("src/include/header.php"); ?>

    <a href="checkout.php" class="buy">
      買sedfood抽BMW
    </a>
    <header id="section_header">
      <div class="slider-wrap">
        <div class="slider main-slider">
         <div>
           <div class="img-wrap">
             <img src="src/dist/img/banner.png" alt="">
           </div>
         </div>
         <div>
           <div class="img-wrap">
             <img src="src/dist/img/banner-2.jpg" alt="">
           </div>
         </div>

        </div>
      </div>
      </header>
    <div class="menu-wrapper">
      <nav class="main-nav" style="width: 100%;">
        <ul class="list-unstyled">
            <li class="menu_item_effect">
              <a href="#draw-info">
                抽獎說明
              </a>
            </li>
            <li class="menu_item_effect">
              <a href="#section_prize">
                獎品項目
              </a>
            </li>
    
            <li class="menu_item_effect">
              <a href="#section_luckybag_content" >
                福袋內容
              </a>
            </li>
            <li class="menu_item_effect">
              <a href="#section_rule">
                活動規則
              </a>
            </li>
            <li class="menu_item_effect">
              <a href="#section_contact">
                聯絡我們
              </a>
            </li>
          </ul>  
      </nav>
    </div>

    <section id="draw-info">
      <section id="section_chance" style="opacity: 1;" class="animated fadeInUp">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="title text-center">
                <h5>手機中獎機率</h5>
              </div>
              <div class="txt text-center">
                <span>1000分之</span>
                <span>1</span>
                <p class="forth-color">*首波抽獎獎品機率</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="section_counting" style="opacity: 1;" class="animated fadeInUp">
        <div class="container">
          <div class="row">
            <div class="col-12 col-sm-12  col-md-3 d-flex align-items-end">
              <span class="title">距離結束時間</span>
            </div>
            <div class="col-12 col-sm-12 col-md-9">
              <div class="timer d-flex d-column align-items-end justify-content-between">
                <!-- <span class="title">距離結束時間</span> -->
                <span class="clock-builder-output"></span>
              </div> 
            </div>

             
          </div>
        </div>
      </section>

      <section id="section_include">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="title text-center">
                <h5>989包括</h5>
              </div>
              <div class="content">
                <div class="item pb-4">
                  <span class="tag">第一重</span>
                  <span class="txt">當購買份數達到5000份時，屆時會在臉書公告抽獎時間，於公告時間直播，於直播中抽出5名ihone得主</span>
                  <span class="open-btn">▼點我查看更多</span>
                  <div class="panel p-2">當購買份數達到5000份時，屆時會在臉書公告抽獎時間，於公告時間直播，並於直播中抽出5名ihone得主。
                  <br>
                  ＊直播當場會聯繫得獎人，若得獎人沒有接電話，則視同棄權，屆時會依序抽出下一位得主。
                  <br>
                  ＊得獎的序號無法再進行下一輪的抽獎，若購買人購買兩份，則另外一個序號依舊可以進行下一輪獎項抽獎，依此類推。
                  </div>
                </div>
                <div class="item pb-4">
                  <span class="tag">第二重</span>
                  <span class="txt">當購買份數達到10000份時，屆時又會在臉書公告抽獎時間，於公告時間直播，於直播中抽出2名桃園-東京來回機票兩張的得主</span>
                  <span class="open-btn">▼點我查看更多</span>
                  <div class="panel p-2">當購買份數達到10000份時，屆時會在臉書公告抽獎時間，於公告時間直播，並於直播中抽出出2名桃園-東京來回機票兩張的得主。
                    <br>
                  ＊直播當場會聯繫得獎人，若得獎人沒有接電話，則視同棄權，屆時會依序抽出下一位得主。
                  <br>
                  ＊得獎的序號無法再進行下一輪的抽獎，若購買人購買兩份，則另外一個序號依舊可以進行下一輪獎項抽獎，依此類推。
                  </div>
                </div>
                <div class="item pb-4">
                 <span class="tag">第三重</span>
                 <span class="txt">抽出眾所矚目的最大獎：BMW 330i sport!!!於臉書粉絲團公告抽獎時間，並會找一位在網路上抹有聲量的主持人抽出這個最大獎，現場並邀請律師到場見證。</span>
                 <span class="open-btn">▼點我查看更多</span>
                 <div class="panel p-2">抽出眾所矚目的最大獎：BMW 330i sport!!!於臉書粉絲團公告抽獎時間，並會找一位在網路上抹有聲量的主持人抽出這個最大獎，現場並邀請律師到場見證。
                   <br>
                 ＊直播當場會聯繫得獎人，若得獎人沒有接電話，則視同棄權，屆時會依序抽出下一位得主。
                 <br>
                 ＊得獎的序號無法再進行下一輪的抽獎，若購買人購買兩份，則另外一個序號依舊可以進行下一輪獎項抽獎，依此類推。
                 </div>
               </div>
              </div>
            </div>
          </div>  
        </div>
      </section>

      <section id="section_draw">
        <div class="container">
          <div class="row">
              <div class="col-12">
                <div class="title text-center">
                    <h5>抽獎規則</h5>
                  </div>
              </div> 
              <div class="col-12 col-sm-6">
                  <div class="img-wrapper">
                    <img src="src/dist/img/draw-pic.png" class="img-fluid">
                  </div>  
              </div>
              <div class="col-12 col-sm-6 d-flex align-items-center">
                  <div class="content">
                      <div class="txt">於臉書粉絲團公告抽獎直播時間，屆時會邀請在網路上具有聲量的人物，當場抽出中獎人。</div>
                      <div class="panel">並且會在直播中直接連繫中獎人，若中獎人沒有接到該電話，則視同棄權，會依序抽出下一位中獎人。
                      <br>
                      ＊之後也會有中獎人領獎的畫面於臉書粉絲團做直播。</div>
                      <span class="close-btn">▼點我查看更多</span>

                  </div>
              </div>

          </div>  
        </div>
      </section>
    </section>
    
  
    <section id="section_prize">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="prize">
              <div class="title text-center">
                <h5>特獎</h5>
              </div>
              <div class="row">
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="txt text-center">
                    <h5>特獎一名</h5>
                    <p>BMW 330i M Sport</p>
                  </div>
  
                </div>
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="img-wrapper p-4">
                    <img src="src/dist/img/BMW.png" class="img-fluid">
                  </div>  
                </div>
              </div>
                
            </div> 
          </div>  
          <div class="col-12 col-sm-12 col-md-6">
            <div class="prize mt-4">
              <div class="title text-center">
                <h5>頭獎</h5>
              </div>
              <div class="row p-2">
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="txt text-center">
                    <h5>頭獎兩名</h5>
                    <p>東京-台北來回機票兩張（不含稅）</p>
                  </div>
          
                </div>
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="img-wrapper p-4">
                    <img src="src/dist/img/ticket.png" class="img-fluid">
                  </div>  
                </div>
              </div>
                
            </div> 
          </div>

          <div class="col-12 col-sm-12 col-md-6">
            <div class="prize mt-4">
              <div class="title text-center">
                <h5>貳獎</h5>
              </div>
              <div class="row p-2">
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="txt text-center">
                    <h5>貳獎五名</h5>
                    <p>iphone-xs-max 256g</p>
                  </div>
  
                </div>
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="img-wrapper p-4">
                    <img src="src/dist/img/iphone.png" class="img-fluid">
                  </div>  
                </div>
              </div>
                
            </div> 
          </div>

          <div class="col-12 col-sm-4 d-none">
            <div class="prize mt-4">
              <div class="title text-center">
                <h5>特獎</h5>
              </div>
              <div class="row p-4">
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="txt text-center">
                    <h5>特獎一名</h5>
                    <p>Mercedes-Benz A-Class A180</p>
                  </div>
  
                </div>
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="img-wrapper p-2">
                    <img src="src/dist/img/prize-item.png" class="img-fluid">
                  </div>  
                </div>
              </div>
                
            </div> 
          </div>
          <div class="col-12 col-sm-4 d-none">
            <div class="prize mt-4">
              <div class="title text-center">
                <h5>特獎</h5>
              </div>
              <div class="row p-4">
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="txt text-center">
                    <h5>特獎一名</h5>
                    <p>Mercedes-Benz A-Class A180</p>
                  </div>
  
                </div>
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="img-wrapper p-2">
                    <img src="src/dist/img/prize-item.png" class="img-fluid">
                  </div>  
                </div>
              </div>
                
            </div> 
          </div>
          <div class="col-12 col-sm-4 d-none">
            <div class="prize mt-4">
              <div class="title text-center">
                <h5>特獎</h5>
              </div>
              <div class="row p-4">
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="txt text-center">
                    <h5>特獎一名</h5>
                    <p>Mercedes-Benz A-Class A180</p>
                  </div>
  
                </div>
                <div class="col-6 d-flex align-items-center justify-content-center">
                  <div class="img-wrapper p-2">
                    <img src="src/dist/img/prize-item.png" class="img-fluid">
                  </div>  
                </div>
              </div>
                
            </div> 
          </div>
        </div>
      </div>  
    </section>

    <section id="section_prize_2" class="d-none">
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="title text-center">
              <h5>汽車中獎機率</h5>
            </div>  
          </div>

          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div> 
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div>  
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div>
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div>   
        </div>
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="title text-center">
              <h5>汽車中獎機率</h5>
            </div>  
          </div>

          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div> 
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div>  
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div> 
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div> 
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div>  
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div>
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div>  
          <div class="col-8 col-auto m-auto col-sm-3 text-center pt-5">
            <h6>SHARP 40型 FHD智慧連網顯示器</h6>
            <div class="img-wrapper">
              <img src="src/dist/img/tv_img.png" class="img-fluid">
            </div>  
            <div class="tag text-center">五名</div>
          </div>  
        </div>
      </div>  
    </section>

    <section id="section_luckybag_content">
      <div class="container">
        <div class="row">
            <div class="col-12">
              <div class="title text-center">
                <h5>福袋內容物</h5>
              </div>
            </div> 
            <div class="col-12 col-sm-12 col-md-6 gallery-slider">
               <div class="bigImage">
                 <img src="src/dist/img/luckyBag/allproduct-B.jpg" alt="" id="bigImage">
               </div>

               <div class="caresoul">
                 <div><img src="src/dist/img/luckyBag/shrimp.jpg" alt=""></div>
                 <div><img src="src/dist/img/luckyBag/squid.jpg" alt=""></div>
                 <div><img src="src/dist/img/luckyBag/fish.jpg" alt=""></div>
                 <div><img src="src/dist/img/luckyBag/Scallop.jpg" alt=""></div>
                 <div><img src="src/dist/img/luckyBag/allproduct-B.jpg" alt=""></div>

               </div> 
            </div>
            <div class="col-12 col-sm-12 col-md-6 d-flex align-items-start flex-column">
              <div class="content">
                <div class="content-set">
                      <h5 class="pb-4">福袋超值海鮮組A</h5>
                      <ul class="list-unstyled feature-list">
                        <li>草蝦12隻 300g ± 10g</li>
                        <li>鹹魚片1條 300g</li>
                        <li>小干貝1包 300g</li>
                        <li>帶殼節孔貝1包 300g</li>
                        <li>鱸魚1條 300g</li>
                      </ul>
                </div>
                <div class="content-set pt-4">
                      <h5 class="pb-4">福袋超值海鮮組B</h5>
                      <ul class="list-unstyled feature-list">
                        <li>草蝦12隻 300g ± 10g</li>
                        <li>透抽2隻 16cm-20cm/隻 150-170g/隻 </li>
                        <li>小干貝1包 300g</li>
                        <li>帶殼節孔貝1包 500g</li>
                      </ul>
                </div>
                <div class="forth-color pt-4">＊組合無法指定，隨機出貨</div>
              </div>
                
            </div>
        </div>  
      </div>
    </section>

    <section id="section_rule">
      <div class="container">
        <div class="row">
            <div class="col-12">
              <div class="title text-center">
                <h5>活動規則</h5>
              </div>
            </div> 
            <div class="col-12 pl-5">
              <ol>
                <li>限定合法擁有中華民國身分證或中華民國護照之民眾參加，請確認合法擁有中華民國身分證或中華民國護照，若無則視同放棄後續領獎資格。</li>

                <li>活動期間：<span class="second-color">108/03/29 12:00 ~ 108/05/10 23:59:59</span>，抽寶馬福袋箱（以下簡稱本活動）單一商品售價：新台幣<span class="second-color">989</span>元整，限量銷售15,000個，售完為止。</li>
                
                <li>每個商品中皆含有抽獎序號，詳細內容請見首頁說明。</li>

                <li>福袋箱內商品為隨機包裝出貨，商品如有不良或短少情形，請致電客服專線<span class="second-color">02-2752-0008</span>（服務時間週一~週五10:00~12:30、14:30~18:00，週六日及國定例假日等非一般正常工作天，恕不提供服務）為您進行瑕疵換貨。</li>

                <li>商品內限量抽獎序號、立馬抽連結皆由訂購資訊提供之Email、手機進行寄送，如有遺失或未收到情形，可來電或私訊粉專進行查詢，若提供之個人資料有誤導致無法查詢或連結無法使用情形，請致電客服專線為您進行服務。</li>

                <li>活動期間於本活動網站，完成訂購且付款完成，可獲得抽獎序號、限量抽獎序號將於<span class="second-color">108/05/11 14:00</span>舉辦現場抽獎活動抽出幸運中獎人，立馬抽連結可立即於本活動網站進行線上抽獎，天天抽電視則會由主辦單位進行抽獎。</li>

                <li>參加者參加本活動同時，即同意接受本活動注意事項、中獎領取、退換貨、隱私權條款之規範，如有違反，愛點子有限公司（以下簡稱主辦單位）查證屬實後有權取消其參加與中獎資格。</li>

                <li>所有抽獎資格為獨立擁有，不限個人名額，故同一消費者，若完成購買2個商品，即代表獲得2個限量抽獎、更多數量則以此類推。</li>

                <li>限量抽獎、立馬抽之獎項，數量與詳細內容請見說明。</li>

                <li>獎項或獎品一旦寄出後，若有遺失、遭冒領或偷竊等喪失佔有之情形，主辦單位恕不負責補發。</li>

                <li>若中獎者不符合、不同意或違反本活動規定者，主辦單位保有取消中獎資格及參與本活動的權利，並對任何破壞本活動行為保留法律追訴權。</li>

                <li>獎項內容及服務品質應參照生產商或服務商提供之標準，如因使用該獎品服務產生任何爭議，由該獎項或服務之提供廠商依法承擔責任。</li>

                <li>中獎者若未滿20歲，須獲得法定代理人同意及代為領取。依中華民國所得法規規定，舉凡中獎金額或獎項年度累計價值超過1,001元（含）以上，將列入本年度之個人綜合所得稅申報，本活動主辦單位將於次年度開立扣繳憑單寄送予中獎者，中獎者於兌獎時需繳交身份證正反面影本。另中獎獎品價值超過新台幣20,000元（含）以上者，應自行負擔機會中獎所得稅，依法須預先扣繳10%稅金（稅額以獎品市價計算），未能如期依法繳納應繳稅額者，視同自願放棄中獎權利。</li>

                <li>本活動無任何線下實體銷售通路，特此說明。</li>

                <li>如遇不可抗力之因素，主辦單位保留隨時修改活動辦法及更換等值獎品之權利</li>
              </ol>

                <!-- <div class="content">
                  <h4 class="pb-4 pt-4 second-color">週週抽機會</h4>
                  <p>活動期間：2019/03/25~05/24，就發發福袋之【988元買牛排送購屋基金】單一售價：新台幣988元整。</p>
                  <br>
                  <ol>
                    <li>活動期間：2019/03/25~05/24，就發發福袋之【988元買牛排送購屋基金】單一售價：新台幣988元整。</li>
                    <li>988元買牛排送購屋基金】限量銷售1萬5千份，售完為止，以下簡稱【特規牛排福袋組合】。</li>
                    <li>【特規牛排福袋組合】分別有3款，出貨時隨機出貨，恕不供選擇，牛排內容說明如下。</li>
                  </ol>
                </div> -->
            </div>

        </div>  
      </div>
    </section>

    <section id="section_contact">
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-6">
            <div class="content d-flex flex-column align-items-center">
              <div class="title pb-4">
                <h5>客服專線</h5>
              </div>
              <a href="tel:02-77515072">
                <h4>
                  02-27520008
                </h4>
              </a>
              <ul class="list-unstyled pt-4">
                <li>
                  <span>09:00 - 12:30 / 13:30 - 18:00</span>
                </li>
                <span class="forth-color notice">服務時間為禮拜ㄧ ~ 禮拜五 不含週休二日 </span>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>


       <?php include("src/include/footer.php"); ?>
